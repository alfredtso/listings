# Motivation
- To get listings from JPX 
- and HKEX [Shanghai/Shenzhen] Connect "Northbound Trading" eligible securities daily and diff those in the database, running it daily
- HKEX publishs listing in [HKEX](https://www.hkex.com.hk/Mutual-Market/Stock-Connect/Eligible-Stocks/View-All-Eligible-Securities?sc_lang=en)
- JPX has listings all over the website

# JPX
- The script now parse 5 tables:
  1. New listing of Stocks
  2. REIT listings
  3. ETF listings
  4. ETN listings
  5. Leveraged Inverse Product listings

# HKEX
- The script parses 2 csv
  - SSE Northbound trading securities
  - SZSE Northbound trading securities

# MySql
- Used `mysql.connector` to talk to the database

# TODO:
- Consider not using `beautifulsoup4` for `jpx.py` as it might require host to install `html.parser`


# Daily task: Add to crontab / 
```shell
crontab -e 

* * * * * /usr/bin/python3 hkex_sse_szse_connect_listings.py sz >> sz.txt
* * * * * /usr/bin/python3 hkex_sse_szse_connect_listings.py sh >> sh.txt
```
