import pandas as pd
import requests
import io
import click
from pathlib import Path
import mysql.connector
from contextlib import closing
from dotenv import dotenv_values

# url of the said csv file
SHANGHAI_CONNECT_URL = "https://www.hkex.com.hk/-/media/HKEX-Market/Mutual-Market/Stock-Connect/Eligible-Stocks/View-All-Eligible-Securities/SSE_Securities.csv?la=en"
SHENZHEN_CONNECT_URL = "https://www.hkex.com.hk/-/media/HKEX-Market/Mutual-Market/Stock-Connect/Eligible-Stocks/View-All-Eligible-Securities/SZSE_Securities.csv?la=en"
CSV_PATH = Path('.') / 'csv'

# Columns of interest
SZSE = ['SZSE Stock Code', 'CCASS Stock Code', 'Stock Name', 'Face Value (RMB)']
SSE = ['SSE Stock Code', 'CCASS Stock Code', 'Stock Name', 'Face Value (RMB)']


# TODO: cli arg
# MySQL ip
UAT = '10.0.1.247'

# db config
db_config = dotenv_values(".env")

def mysql_query(query: str):
    with closing(mysql.connector.connect(**db_config)) as cnx:
        with closing(cnx.cursor()) as cur:
            cur.execute(query)
            res = cur.fetchall()

    return res

@click.group()
def cli():
    pass

@click.command()
def sh():
    click.echo("Get Shanghai Connect New Listing")
    return fetch_hkex_csv_with_requests(SHANGHAI_CONNECT_URL)

@click.command()
def sz():
    click.echo("Get Shenzhen Connect New Listing")
    return fetch_hkex_csv_with_requests(SHENZHEN_CONNECT_URL, cols=SZSE)

@click.command()
def all():
    click.echo("Get all Listing")
    fetch_hkex_csv_with_requests(SHANGHAI_CONNECT_URL)
    fetch_hkex_csv_with_requests(SHENZHEN_CONNECT_URL, cols=SZSE)

def fetch_hkex_csv_with_requests(csv_url=SHANGHAI_CONNECT_URL, cols=SSE) -> pd.DataFrame:
    request = requests.get(csv_url)

    # find the number of row to skip
    lines_of_csv = request.text.split('\n')
    # TODO: stop flag better alternatives
    stop_flag = False
    rows_to_skip = 0
    for i, fields in enumerate(lines_of_csv):

        # Get skip row
        if not stop_flag:
            # Get the update date
            if "Updated" in fields:
                print(fields)

            # the wording is not guaranteed
            if "following Northbound trading day" in fields:
                rows_to_skip = i + 1
                print(f'skip {rows_to_skip} rows')
                stop_flag = True

    # make a df
    df = pd.read_csv(io.StringIO(request.text), skiprows=rows_to_skip, sep='\t', index_col=0)
    df = df[cols]

    print(df.info())

    return df

# adding commands
cli.add_command(sh)
cli.add_command(sz)
cli.add_command(all)

if __name__ == '__main__':
    cli()
