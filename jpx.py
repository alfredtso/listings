import pandas as pd
from bs4 import BeautifulSoup
import requests
import mysql.connector
from contextlib import closing
from dotenv import dotenv_values

# TODO: Sitemap get websites

NEW_STOCKS = 'https://www.jpx.co.jp/english/listing/stocks/new/index.html'
REIT_ISSUES = 'https://www.jpx.co.jp/english/equities/products/reits/issues/index.html'
ETF_ISSUES = 'https://www.jpx.co.jp/english/equities/products/etfs/issues/01.html'
ETN_ISSUES = 'https://www.jpx.co.jp/english/equities/products/etns/issues/01.html'
LEVERAGED_INVERSE = 'https://www.jpx.co.jp/english/equities/products/etns/leveraged-inverse/01.html'

# TODO: cli arg
# MySQL ip
UAT = '10.0.1.247'

# db config
db_config = dotenv_values(".env")


def _mysql_query(query: str):
    with closing(mysql.connector.connect(**db_config)) as cnx:
        with closing(cnx.cursor()) as cur:
            cur.execute(query)
            res = cur.fetchall()

    return res


def _helper_add_date_column(df: pd.DataFrame, col_to_change: str, dt_format: str):
    df['Date'] = pd.to_datetime(df[col_to_change], format=dt_format)
    return df


def _get_tickers_from_universe(query: str):
    res = _mysql_query(query)
    all_tickers = [t[0] for t in res]
    return all_tickers


def etfs_get_listings_before_today():

    # get from website
    df = _parse_etfs_issues()

    # get rows
    df['Date'] = pd.to_datetime(df["Listing Date"])
    tmp_df = df[df['Date'] < pd.to_datetime("today")]
    new_list = tmp_df['Code'].tolist()

    # get list from universe
    query = 'SELECT ticker FROM qtt.universe WHERE ticker LIKE "% JT Equity"'
    all_tickers = _get_tickers_from_universe(query)

    # add suffix to new_list before comparing
    new_list = [f'{t} JT Equity' for t in new_list]

    # get diff
    to_add_list = []
    for t in new_list:
        if t not in all_tickers:
            to_add_list.append(t)

    print(to_add_list)
    return to_add_list


def stock_get_listings_before_today():

    # get from website
    df = _parse_stocks_issues()

    # get rows
    df = _helper_add_date_column(df, "Date of Listing", "%b. %d, %Y")
    tmp_df = df[df['Date'] < pd.to_datetime("today")]
    new_list = tmp_df['Code'].tolist()

    # add suffix
    new_list = [ f'{t} JT Equity' for t in new_list]

    # remove possible duplicate
    new_list_set = set(new_list)

    # get list from universe
    query = 'SELECT ticker FROM qtt.universe WHERE ticker LIKE "% JT Equity"'
    all_tickers = _get_tickers_from_universe(query)
    all_tickers_set = set(all_tickers)

    # get diff
    to_add_list = [t for t in new_list_set if t not in all_tickers_set]

    print(to_add_list)
    return to_add_list


def _helper_parse_table(url=NEW_STOCKS, drop_cols=None):
    page = requests.get(url)
    soup = BeautifulSoup(page.text, "html.parser")

    table = soup.find('table')
    table_rows = table.find_all('tr')

    # To skip the one entry table
    while len(table_rows) == 1:
        table = table.find_next('table')
        table_rows = table.find_all('tr')

    # Header
    ths = table.find_all('th')
    header = [th.text.strip() for th in ths if th.text.strip()]

    # Rows
    rows = []
    for tr in table_rows:

        # Columns
        td = tr.find_all('td')
        # drop empty entries
        row = [tr.text.strip() for tr in td if tr]

        if row:
            rows.append(row)
            num_of_cols = len(row)

    # the slicing is for handling tr without text like pdf icons links
    df = pd.DataFrame(rows, columns=header[:num_of_cols])

    if drop_cols:
        df.drop(drop_cols, axis=1, inplace=True)

    print("Successfully parsed table")
    return df


def _parse_reits_issues(url=REIT_ISSUES) -> pd.DataFrame:
    """
    Parse the ETFs page, return a Dataframe

    :return: pd.DataFrame
    """
    return _helper_parse_table(url=url)


def _parse_etfs_issues(url=ETF_ISSUES) -> pd.DataFrame:
    """
    Parse the ETFs page, return a Dataframe

    :return: pd.DataFrame
    """
    return _helper_parse_table(url=url, drop_cols=["Market Maker(*)"])


def _parse_stocks_issues() -> pd.DataFrame:
    """
    Parse the Stocks page, return a Dataframe

    :return: pd.DataFrame
    """
    return _helper_parse_table()


def _parse_etns_issues() -> pd.DataFrame:
    """
    Parse the ETNs page, return a Dataframe

    :return: pd.DataFrame
    """
    return _helper_parse_table(url=ETN_ISSUES)


def _parse_leveraged_inverse_issues() -> pd.DataFrame:
    """
    Parse the Leveraged and Inverse page, return a Dataframe

    :return: pd.DataFrame
    """
    return _helper_parse_table(url=LEVERAGED_INVERSE)

if __name__ == "__main__":
    df = _parse_stocks_issues()
    stock_get_listings_before_today(df)
